#include <avr/io.h>
#include <util/delay.h>

#include "lcd_4094.c"

int main (void) {
  // B0, B1, B2 as outputs
  DDRB = 0x07;

  LCD_Init();

  clearBit(PORTB, CD4094_CLOCK);
  setBit(PORTB, CD4094_DATA);
  clearBit(PORTB, CD4094_STROBE);

  msDelay(500);

  LCD_Message("Hello Manolo!");

  // custom char implementation
  unsigned char happy_dude[] = { 0x0E, 0x0E, 0x04, 0x1F, 0x04, 0x04, 0x0A, 0x0A };
  customChar(happy_dude, 0);

  LCD_Goto(0,1);
  LCD_Char((BYTE)0x00);
  
}
