#include <avr/io.h>
#include <util/delay.h>
#include <string.h>
#include <stdlib.h>

#define setBit(port, bit) port |= (1 << bit)
#define clearBit(port, bit) port &= ~(1 << bit)

#define CD4094_DATA    0
#define CD4094_STROBE  1
#define CD4094_CLOCK   2

#define CLEARDISPLAY 0x01
#define SETCURSOR    0x80

typedef uint8_t BYTE;
BYTE state = 0x00;

void msDelay (int delay) {
  for (int i = 0; i < delay; i++) {
    _delay_ms(1);
  }
}

void write_byte (BYTE data) {
  BYTE bit;
  for (bit = 0; bit < 8; bit++) {
    if (data & 0x80) {
	setBit(PORTB, CD4094_DATA);
    } else {
      clearBit(PORTB, CD4094_DATA);
    }

    setBit(PORTB, CD4094_CLOCK);
    clearBit(PORTB, CD4094_CLOCK);
    data <<= 1;
  }

  setBit(PORTB, CD4094_STROBE);
  msDelay(1);
  clearBit(PORTB, CD4094_STROBE);
  msDelay(1);
}

void pulseEnableLine (void) {
  // E pulse
  state |= (1 << 2);
  write_byte(state);
  msDelay(2);
  // clear line
  state &= ~(1 << 2);
  write_byte(state);
}

void sendNibble (BYTE data) {
  // clear line
  state &= ~(1 << 7);
  state &= ~(1 << 6);
  state &= ~(1 << 5);
  state &= ~(1 << 4);
  
  write_byte(state);

  state |= data & 0xF0;
  write_byte(state);
  pulseEnableLine();
}

void sendByte (BYTE data) {
  sendNibble(data);
  sendNibble(data << 4);
}

void LCD_Cmd (BYTE cmd) {
  // clear RS
  state &= ~(1 << 3);
  sendByte(cmd);
}

void LCD_Char (BYTE ch) {
  // set Rs
  state |= (1 << 3);
  sendByte(ch);
}

void LCD_Home (void) {
  LCD_Cmd(SETCURSOR);
}

void LCD_Clear (void) {
  LCD_Cmd(CLEARDISPLAY);
  msDelay(2);
}

void LCD_Goto (BYTE x, BYTE y) {
  BYTE addr = 0;
  switch (y) {
  case 1: addr = 0x40; break;
  case 2: addr = 0x14; break;
  case 3: addr = 0x54; break;
  }
  LCD_Cmd(SETCURSOR+addr+x);
}

void LCD_Line (BYTE row) {
  LCD_Goto(0, row);
}

void LCD_Message (const char *text) {
  while (*text) {
    LCD_Char(*text++);
  }
}

void customChar (unsigned char *pattern, const char location) {
  LCD_Cmd(0x40+(location*8));
  for (int i = 0; i < 8; i++) {
    LCD_Char(pattern[i]);
  }
}

void LCD_Init (void) {
  LCD_Cmd(0x33); // initialize controller
  LCD_Cmd(0x32); // set to 4-bit input mode
  LCD_Cmd(0x28); // 2 line, 5x7 matrix
  LCD_Cmd(0x0C); // turn cursor off (0x0E to enable)
  LCD_Cmd(0x06); // cursor direction = right
  LCD_Cmd(0x01); // start with clear display
  msDelay(2);
}

