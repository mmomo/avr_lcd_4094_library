DEVICE     = attiny85
PROGRAMMER = usbasp
FILENAME   = main
COMPILE    = avr-gcc -g -Os -mmcu=$(DEVICE)

all: build upload clean

build:
	$(COMPILE) -c $(FILENAME).c -o $(FILENAME).o
	$(COMPILE) -o $(FILENAME).elf $(FILENAME).o
	avr-objcopy -j .text -j .data -O ihex $(FILENAME).elf $(FILENAME).hex
	avr-size --format=avr --mcu=$(DEVICE) $(FILENAME).elf

upload:
	avrdude -c $(PROGRAMMER) -v -p $(DEVICE) -U flash:w:$(FILENAME).hex:i 

clean:
	rm main.o
	rm main.elf
	rm main.hex
